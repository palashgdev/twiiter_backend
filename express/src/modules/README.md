All the modules follows the MVC structure in which
  1. routes.js
    contain the all the routes for the modules
  1. model.js
    contain all the schema that will be used for the modules
  1. controller.js
    maintain the control section for all the routes
  1. [module_name].js
    contain all the database function that will be used for the modules