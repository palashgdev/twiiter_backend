import * as Yup from 'yup';
import { hashSync, compareSync, genSaltSync } from 'bcrypt-nodejs';
import { CreateUser, get, addFollower, unFollow, getAllFollowing, getAllFollowers } from "../user"
import { AuthServices } from "../../../services/auth";
import User from "../model";

function authentication(password, comparepassword) {
  return compareSync(password, comparepassword);
}

//function to creating the user
export const create = async (req, res) => {
  try {
    const { fullName, email, username, password } = req.body;
    const [firstName, ...lastName] = fullName.split(' ');

    const bodySchema = Yup.object().shape({
      firstName: Yup.string().required(),
      lastName: Yup.string().required(),
      email: Yup.string().email().required(),
      username: Yup.string().required(),
      password: Yup.string().required()
    });

    //validate the payload
    await bodySchema.validate({
      firstName,
      lastName,
      email,
      username,
      password
    });

    const response = await get({ username });

    console.log(response);
    //if user not found
    if (response.statusCode == 404) {
      const res1 = await CreateUser({
        username,
        email,
        firstName,
        lastName,
        password
      });

      //if user created
      if (res1.statusCode === 201) {
        const jwtoken = AuthServices.createToken(res1.payload);

        res.status(201).json({
          statusCode: 201,
          message: "Success",
          payload: {
            token: jwtoken,
            id: res1.payload._id
          }
        });
      } else {
        res.status(res1.statusCode).json({
          error: res1.error,
          statusCode: res1.statusCode
        })
      }
    } else {
      res.status(401).json({
        error: "User Already Present",
        statusCode: 401
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    });
  }
};

//login function
export const login = async (req, res) => {
  try {
    //check if the user contain the token via req.user
    if (req.user) {
      const { username, password } = req.body;

      const bodySchema = Yup.object().shape({
        username: Yup.string().required(),
        password: Yup.string().required()
      });

      await bodySchema.validate({ username, password });


      const user = await get({ username });

      //user found
      if (user.statusCode === 200) {

        //check passoword is corrent or not
        if (!authentication(password, user.payload.password)) {
          res.status(401).json({
            statusCode: 401,
            error: "Password does not match"
          });
        }

        //validate the token of user itself
        if (user.payload._id.toString() == req.user._id.toString()) {
          res.status(200).json({
            statusCode: 200,
            message: "Success"
          });
        } else {
          res.status(401).json({
            statusCode: 401,
            error: "Invalid Token"
          });
        }

      } else {
        res.status(user.statusCode).json({
          error: user.error,
          statusCode: user.statusCode
        });
      }
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    });
  }
};


//this function will follow the user
export const follow = async (req, res) => {
  try {
    const { follower, following } = req.body;

    const bodySchema = Yup.object().shape({
      follower: Yup.string().required(),
      following: Yup.string().required()
    })

    await bodySchema.validate({ follower, following });

    const response = await addFollower({
      username: follower,
      followinguser: following
    });

    if (response.statusCode === 200) {
      res.status(200).json({
        statusCode: response.statusCode,
        message: response.message
      });
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      });
    }

  } catch (error) {
    res.status(500).json({ statusCode: 500, error: "Server Error" });
  }
};


//this user will unfollow the user
export const unfollows = async (req, res) => {
  try {
    const { unfollower, unfollowing } = req.body;
    const bodySchema = Yup.object().shape({
      unfollower: Yup.string().required(),
      unfollowing: Yup.string().required()
    })

    await bodySchema.validate({ unfollower, unfollowing });

    const response = await unFollow({ username: unfollower, unFollowingUsername: unfollowing });

    if (response.statusCode === 200) {

      res.status(response.statusCode).json({
        statusCode: response.statusCode
        , message: response.message
      });

    } else {

      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      });

    }
  } catch (error) {
    res.status(500).json({ statusCode: 500, error: "Server Error" })
  }
};

//function that will get the no of the follower of the user
export const nofollower = async (req, res) => {
  try {
    const { username } = req.params;

    const bodySchema = Yup.object().shape({
      username: Yup.string().required()
    });

    await bodySchema.validate({ username });

    const response = await getAllFollowers({ username });

    if (response.statusCode === 200) {
      res.status(200).json({
        statusCode: 200,
        message: response.message,
        payload: response.payload
      })
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
};

//function that get all the following of the user
export const noFollowing = async (req, res) => {
  try {
    const { username } = req.params;

    const bodySchema = Yup.object().shape({
      username: Yup.string().required()
    });

    await bodySchema.validate({ username });

    const response = await getAllFollowing({ username });

    if (response.statusCode === 200) {
      res.status(200).json({
        statusCode: response.statusCode,
        message: response.message,
        payload: response.payload
      })
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
};