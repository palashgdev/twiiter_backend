// This file contain all the database function related to the user

import User from './model';
import mongoose from "mongoose";

/**
 * @async
 * @function CreateUser
 * @description This function will create the user
 * @author Palash
 * @param {String} username
 * @param {String} email
 * @param {String} firstName
 * @param {String} lastName
 * @param {String} password
 * @returns Promise
 * @example
 * CreateUser({
 *  username: "rammu",
 *  email: "rammu@gmail.com",
 *  lastName: "kaka",
 *  firstName: "rammu",
 *  password: "rammu"
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function CreateUser({ username, email, firstName, lastName, password }) {
  try {

    const _user = await User.create({
      username,
      email,
      firstName,
      lastName,
      password
    });

    return {
      statusCode: 201,
      message: "Success",
      payload: _user
    };

  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// CreateUser({
//   username: "palashg7563",
//   email: "palashg7563@gmail.com",
//   lastName: "gupta",
//   firstName: "palash",
//   password: "palash"
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));


/**
 * @async
 * @function get
 * @description This function get a user otherwise not
 * @author Palash
 * @param {String} username
 * @returns Promise
 * @example
 * get({ username: "palashg7563" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function get({ username }) {
  try {
    const user = await User.findOne({ username });

    if (!user) {
      return {
        statusCode: 404,
        error: "User Not Found"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: user
    }
  } catch (error) {
    throw error;
  }
}

// get({ username: "rammu" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @todo check the userexist or not before and after adding the follower
 * @async
 * @function addFollower
 * @description This function add the follower i.e. follow the us
 * @author Palash
 * @param {String} username
 * @param {String} followinguser
 * @returns Promise
 * @example
 * addFollower({ username: "palashg7563", followinguser: "himanshu98" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function addFollower({ username, followinguser }) {
  try {
    if (username == followinguser) {
      return {
        statusCode: 301,
        message: "User cannot follow itself"
      }
    }
    const followerUser = await User.findOne({ username });
    const followingUsers = await User.findOne({ username: followinguser });

    if (!followerUser || !followingUsers) {
      return {
        statusCode: 404,
        error: "User Not Found"
      }
    };

    if (followerUser["following"].indexOf(mongoose.Types.ObjectId(followingUsers._id)) == -1) {
      const following = [...followerUser.following, followingUsers._id];
      const follower = [...followingUsers.follower, followerUser._id];

      const res = await User.update({ username }, { following });
      const res1 = await User.update({ username: followinguser }, { follower });

      if (res.n == 1 && res1.n == 1) {
        return {
          statusCode: 200,
          message: "User Followed"
        }
      } else {
        return {
          statusCode: 500,
          message: "Something wrong happen"
        }
      }
    } else {
      return {
        statusCode: 401,
        error: "User Already Followed"
      }
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
};

// addFollower({ username: "palashg7563", followinguser: "rammu" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));


/**
 * @async
 * @function unFollow
 * @description This function will unfollow the user
 * @author Palash
 * @param {String} username
 * @param {String} unFollowingUsername
 * @returns Promise
 * @example
 * unFollow({ username: "palashg7563", unFollowingUsername: "himanshu98" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function unFollow({ username, unFollowingUsername }) {
  try {
    const unfollower = await User.findOne({ username });

    const unfollowing = await User.findOne({ username: unFollowingUsername });

    if (!unfollower || !unfollowing) {
      return {
        statusCode: 404,
        error: "User Not Found"
      }
    }

    if (unfollower["following"].indexOf(mongoose.Types.ObjectId(unfollowing._id)) == -1) {
      return {
        statusCode: 401,
        error: "User Already Unfollowed"
      }
    }
    const following = [
      ...unfollower["following"].slice(0, unfollower["following"].indexOf(mongoose.Types.ObjectId(unfollowing._id)))
      , ...unfollower["following"].slice(1 + unfollower["following"].indexOf(mongoose.Types.ObjectId(unfollowing._id)))
    ];

    const follower = [
      ...unfollowing["follower"].slice(0, unfollowing["follower"].indexOf(mongoose.Types.ObjectId(unfollower._id))),
      ...unfollowing["follower"].slice(1 + unfollowing["follower"].indexOf(mongoose.Types.ObjectId(unfollower._id)))
    ];

    const res = await User.updateOne({ username }, { following });
    const res1 = await User.updateOne({ username: unFollowingUsername }, { follower });

    if (res.n == 1 && res1.n == 1) {
      return {
        statusCode: 200,
        message: "User unfollowed"
      }
    } else {
      return {
        statusCode: 500,
        error: "Something Happened"
      }
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
};

// unFollow({ username: "palashg7563", unFollowingUsername: "rammu" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @async
 * @function getAllFollowing
 * @description This function will get all following of the user
 * @author Palash
 * @param {String} username
 * @returns Promise
 * @example
 * getAllFollowing({ username:"palasd" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function getAllFollowing({ username }) {
  try {
    const user = await User.findOne({ username })

    if (!user) {
      return {
        statusCode: 404,
        message: "User Does Not Exist"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: user.following
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
};

// getAllFollowing({ username: "palasd" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @async
 * @function getAllFollowers
 * @description This function will getFollowers of the user
 * @author Palash
 * @param {String} username
 * @returns Promise
 * @example
 * getAllFollowers({ username:"palasd" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function getAllFollowers({ username }) {
  try {
    const user = await User.findOne({ username })

    if (!user) {
      return {
        statusCode: 404,
        message: "User Does not Exist"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: user.follower
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// getAllFollowers({ username:"palasd" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));