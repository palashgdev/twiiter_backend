Routes defined here are

1. /api/user/signup (POST request)
1. /api/user/login (POST request)
1. /api/user/follow (POST request)
1. /api/user/unfollow (POST request)
1. /api/user/following/:username (GET request)
1. /api/user/follower/:username (GET request)
