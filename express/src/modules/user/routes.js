import { Router } from "express";
import { userAuth } from "./controller/middleware"

import {
  create,
  login,
  follow,
  unfollows,
  nofollower,
  noFollowing
} from './controller/index'

const routes = Router();

routes.post("/signup", create);
routes.post("/login", userAuth, login);

routes.post("/follow", follow);
routes.post("/unfollow", unfollows);

routes.get("/follower/:username", nofollower);
routes.get("/following/:username", noFollowing);

export default routes;