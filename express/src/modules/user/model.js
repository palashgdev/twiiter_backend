import mongoose, { Schema } from "mongoose";
import { hashSync, compareSync, genSaltSync } from 'bcrypt-nodejs';

import { isDEV, JWT_SECRET } from "../../config/index";

//user's schema
const UserSchema = new Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  email: { type: String },
  firstName: { type: String },
  lastName: { type: String },
  follower: [{
    type: Schema.Types.ObjectId,
    ref: "User"
  }],
  following: [{
    type: Schema.Types.ObjectId,
    ref: "User"
  }],
  password: {
    type: String,
    unique: true,
    required: true,
  }
}, { timestamps: true });


//saving the hashpassword beforing the password of the user in the database
UserSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = this._hashpassword(this.password);
    return next();
  }
  return next();
});

//methods of the USER Schema
UserSchema.methods = {
  _hashpassword(password) {
    const no = genSaltSync(12);
    return hashSync(password, no);
  },
  authentication(password) {
    return compareSync(password, this.password);
  }
};

//indexing for the email of the user is used for the production for better performance
if (!isDEV) {
  UserSchema.index({ username: 1 });
}

export default mongoose.model("User", UserSchema);