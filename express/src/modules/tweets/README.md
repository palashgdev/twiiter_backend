Routes defined here are

1. Create Tweet
  /api/tweet/:userId (POST request)
1. Get a tweet
  /api/tweet/:userId/:tweetId (GET request)
1. Delete a tweet
  /api/tweet/:userId/:tweetId (DELETE request)
1. Get All tweet of the user
  /api/tweet/:userId/ (GET request)