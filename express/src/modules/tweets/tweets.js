//This file will contain all the database function for the tweets
import User from "../user/model";
import Tweet from "../tweets/model";

/**
 * This function will create the tweet before that it check that user exist or not
 * @todo before accessing this function check that this user exist or not
 * @async
 * @function create
 * @param {String} userId
 * @param {String} text
 * @returns Promise
 * @example
 * create({ userId: "5be48f0c24100e2484812a20", text: "This is also a random tweets and i like sharing it" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function create({ userId, text }) {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return {
        statusCode: 404,
        error: "User Not Found"
      }
    }

    const Tweets = await Tweet.create({ text, user: userId });

    return {
      statusCode: 201,
      message: "Success",
      payload: Tweets._id
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
};

// create({ userId: "5be48f0c24100e2484812a20", text: "This is also a random tweets and i like sharing it" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a tweet of a user
 * @async
 * @function get
 * @param {String} tweetId
 * @param {String} userId
 * @returns Promise
 * @example
 * get({ userId: "5be5bf602b8e512f58cf8f92", tweetId: "5be5cfe3d7bfe82bd087adfc" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function get({ userId, tweetId }) {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return {
        statusCode: 404,
        error: "User Does not Found"
      }
    }

    const tweets = await Tweet.findById(tweetId);

    if (!tweets) {
      return {
        statusCode: 404,
        error: "Tweet Not Found"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: tweets
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
}

// get({ userId: "5be5bf602b8e512f58cf8f92", tweetId: "5be5cfe3d7bfe82bd087adfc" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get all the tweets of the user
 * @async
 * @function getTweetsofUser
 * @param {String} userId
 * @returns Promise
 * @example
 * getTweetsofUser({ userId: "5be54fe34ffaa20a2c8a741a" })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function getTweetsofUser({ userId }) {
  try {
    const tweets = await Tweet.find({ user: userId });

    if (tweets.length == 0) {
      return {
        statusCode: 404,
        error: "User Does Not exist"
      }
    }

    return {
      statusCode: 200,
      message: "Success",
      payload: tweets
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Dabase Server Error"
    }
  }
}

// getTweetsofUser({ userId: "5be54fe34ffaa20a2c8a741a" })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will delete the tweet
 * @async
 * @function deleteTweet
 * @param {String} tweetId
 * @returns Promise
 * @example
 * deleteTweet({
 *  userId: "5be5bf602b8e512f58cf8f92",
 *  tweetId: "5be5cfe3d7bfe82bd087adfc"
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function deleteTweet({ userId, tweetId }) {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return {
        statusCode: 404,
        message: "User Not found"
      }
    }

    const tweets = await Tweet.findById(tweetId);

    if (!tweets) {
      return {
        statusCode: 404,
        error: "Not Found"
      }
    }

    const res = await Tweet.deleteOne({ _id: tweetId });

    return {
      statusCode: 200,
      message: "Success"
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
};

// deleteTweet({
//   userId: "5be5bf602b8e512f58cf8f92",
//   tweetId: "5be5cfe3d7bfe82bd087adfc"
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @async
 * @function likeTweet
 * @description This function is used to like a tweet by a user
 * @author Palash
 * @param {String} tweetId
 * @param {String} userId
 * @returns Promise
 * @example
 */
export async function likeTweet({ tweetId, userId }) {
  try {
    const user = await User.findById(userId);

    if (!user) {
      return {
        statusCode: 404,
        error: "Not Found"
      }
    }

    const tweet = await Tweet.findById(tweetId);

    if (!tweet) {
      return {
        statusCode: 404,
        error: "Not Found"
      }
    }

    const likeby = [...tweet.likeby, userId];
    const like = tweet.like + 1;

    const _tweet = await Tweet.updateOne({ _id: tweetId }, { likeby, like });

    if (_tweet.n == 1) {
      return {
        statusCode: 200,
        message: "Success"
      }
    } else {
      return {
        statusCode: 500,
        error: "Something Wrong Happen"
      }
    }
  } catch (error) {
    return {
      statusCode: 500,
      error: "Database Server Error"
    }
  }
};
