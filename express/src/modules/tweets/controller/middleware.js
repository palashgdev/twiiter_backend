//validation of the token is done here as it will be the middleware for the login route
import { AuthServices } from "../../../services/auth";
import User from "../model";

/**
 * This is the middleware that will check the user has the token or not
 * @async
 * @function userAuth
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
export const userAuth = async (req, res, next) => {

  const token = AuthServices.getTokenFromHeaders(req);

  if (!token) {
    req.user = null;
    return res.sendStatus(401);
  }
  const user = await User.findById(token.id);

  if (!user) {
    req.user = null;
    return res.sendStatus(401);
  }
  req.user = user;

  return next();
};