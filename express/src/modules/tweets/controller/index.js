//This file will contain the control function for the routes
import * as Yup from "yup";
import { create, get, deleteTweet, getTweetsofUser } from "../tweets";

//this function will create the tweets
export const creates = async (req, res) => {
  try {
    const { userId } = req.params;
    const { text } = req.body;

    const bodySchema = Yup.object().shape({
      text: Yup.string().required(),
      userId: Yup.string().required()
    });

    await bodySchema.validate({ text, userId });

    const response = await create({ userId, text });
    if (response.statusCode === 201) {
      res.status(201).json({
        statusCode: response.statusCode,
        message: response.message,
        Tweetid: response.payload
      });
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    }

  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    });
  }
}


//this function will get a tweets
export const gets = async (req, res) => {
  try {
    const { tweetId, userId } = req.params;

    const bodySchema = Yup.object().shape({
      tweetId: Yup.string().required(),
      userId: Yup.string().required()
    });

    await bodySchema.validate({ tweetId, userId });

    const response = await get({ userId, tweetId });

    if (response.statusCode === 200) {

      res.status(200).json({
        statusCode: response.statusCode,
        message: response.message,
        payload: response.payload
      });

    } else {
      res.status(response.statusCode)
        .json({
          statusCode: response.statusCode,
          error: response.error
        });
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
};


//this function will delete the tweet's of the user
export const deletetweets = async (req, res) => {
  try {
    const { userId, tweetId } = req.params;

    const bodySchema = Yup.object().shape({
      tweetId: Yup.string().required(),
      userId: Yup.string().required()
    });

    await bodySchema.validate({ tweetId, userId });

    const response = await deleteTweet({ userId, tweetId });

    if (response.statusCode === 200) {
      res.status(200).json({
        statusCode: response.statusCode,
        message: response.message
      });
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error,
      });
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    });
  }
};

export const getAll = async (req, res) => {
  try {
    const { userId } = req.params;

    const bodySchema = Yup.object().shape({
      userId: Yup.string().required()
    });

    await bodySchema.validate({ userId });

    const response = await getTweetsofUser({ userId });

    if (response.statusCode === 200) {
      res.status(response.statusCode).json({
        statusCode: 200,
        message: response.message,
        payload: response.payload
      })
    } else {
      res.status(response.statusCode).json({
        statusCode: response.statusCode,
        error: response.error
      })
    }
  } catch (error) {
    res.status(500).json({
      statusCode: 500,
      error: "Server Error"
    })
  }
}