import express from "express";
import { creates, gets, deletetweets, getAll } from "./controller/index"

const routes = express.Router();

routes.post("/:userId/", creates);
routes.get("/:userId/:tweetId", gets);
routes.get("/:userId", getAll);
routes.delete("/:userId/:tweetId", deletetweets);

export default routes;