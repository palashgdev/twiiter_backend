import mongoose, { Schema } from "mongoose";

const TweetSchema = new Schema({
  text: {
    type: String,
    minlength: [5, "Too Short"],
    maxlength: [400, "Text is too long"]
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  like: {
    type: Number,
    default: 0
  },
  likeby: [{
    type: Schema.Types.ObjectId,
    ref: "User"
  }],
  retweets: [{
    type: Schema.Types.ObjectId,
    ref: "Tweet"
  }]
}, { timestamps: true });

export default mongoose.model("Tweet", TweetSchema);