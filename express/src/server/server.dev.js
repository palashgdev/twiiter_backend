import express from "express";
import { PORT } from '../config/index'
import DB from "../config/db";
import middleware from '../middleware/index';
import routes from "../modules/index";

//app is initialized
const app = express();

//all the middleware are added
middleware(app);

//all the routes are included here
routes(app);

//server is started
app.listen(PORT, (error) => {
  if (error) {
    console.error(error);
  }
  console.log(`Server is started at ${PORT}`);
});