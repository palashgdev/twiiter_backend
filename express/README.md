# Steps to start the project

Following tools or Software are required to run the project
1. docker
1. node.js
1. npm
1. yarn
1. ide (preferably vs_code)

Steps to start the project
1. start the mongo container
    docker container run -p 27017:27017 mongo
    And if you don't contain the image of the mongo docker will automatically download the image for you and bind the 27017 of the localhost to the mongo container.

If you want to run the dev enviroment
run yarn dev
If you want to run the production enviroment
run yarn pro

POSTMAN upadted documentation link
https://documenter.getpostman.com/view/3083371/RzZ9Geyw
